<?php

namespace Bolt\Extension\blockmurder\droptours;

use Bolt\Application;
use Bolt\BaseExtension;

class Extension extends BaseExtension
{


    public function initialize() {
        $end = $this->app['config']->getWhichEnd();

        /*
         * Frontend
         */
        if ($end == 'frontend') {
            // Twig functions
            $this->addTwigFunction('getTours', 'getTours');
            $this->addTwigFunction('firstXtours', 'firstXtours');
        }
    }

    public function getName()
    {
        return "droptours";
    }

    public function getTours()
    {
      $tours = $this->fetchdroptours();
      $types = array();
      $sections = array();

      foreach($tours as $tour){
        if (!in_array($tour['type'], $types)){
          $types[] = $tour['type'];
        }
        if (!in_array($tour['section'], $sections)){
          $sections[] = $tour['section'];
        }
      }

      $result['sections'] = $sections;
      $result['types'] = $types;
      $result['tours'] = $tours;

      return $result;
    }

    public function firstXtours($numberOfentries)
    {
      return $this->fetchdroptours($numberOfentries);
    }

    private function fetchdroptours($limit = 300)
    {
      setlocale(LC_TIME, 'de_CH.UTF-8', 'de_CH.utf8', 'de_DE.UTF-8', 'de_DE.utf8');

      $baseUrl = 'https://ssl.dropnet.ch/sac-huttwil/dropnetapps/tours/api.php';

      $xml = simplexml_load_file($baseUrl."?command=getTours&limit=".$limit) or die("feed not loading");
      $json = json_encode($xml);
      $data = json_decode($json,TRUE);

      foreach ($data['item'] as $tour) {
          $attributes = $tour['@attributes'];
          $addresses = $tour['address'];

          $startDate = date_create_from_format('!Y-m-d', $attributes['date_start']);
          $duration = $this->dateDifference($attributes['date_start'], $attributes['date_end']);

          $item['date'] = $startDate->format('Y-m-d H:i:s');
          $item['type'] = $attributes['category_description'];
          $item['difficulty'] = $attributes['requirements_techn'];
          $item['duration'] = $duration;
          $item['lead'] = $this->getLead($addresses);
          $item['link'] =  $attributes['link'];
          $item['title'] = $attributes['name'];
          $item['section'] = array_filter(explode("|", $attributes['group']));
          $item['status'] = $this->getStatusStr($attributes['tour_status']);

          $details = [];

          $detail['name'] = "Datum";
          $detail['content'] =  strftime("%a %e.%m.%Y", $startDate->getTimestamp())." / ".$duration;
          $details[] = $detail;

          $detail['name'] = "Gruppe";
          $detail['content'] = array_filter(explode("|", $attributes['group']));
          $details[] = $detail;

          $detail['name'] = "Leitung";
          $detail['content'] = $this->getLeadAddress($addresses);
          $details[] = $detail;

          $detail['name'] = "Typ";
          $detail['content'] = $attributes['category_description'];
          $details[] = $detail;

          $detail['name'] = "Anforderung";
          $detail['content'] = $attributes['requirements_techn'];
          $details[] = $detail;

          if($attributes['costs'] != "0" || $attributes['costs_text'] != "")
          {
              $costs = "";
              $costText = "";
              if($attributes['costs'] != "0")
              {
                  $costs = "CHF ".$attributes['costs'].".- / ";
              }

              if($attributes['costs_text'] != "")
              {
                  $costText = $attributes['costs_text'];
              }

              $detail['name'] = "Kosten";
              $detail['content'] = $costs.$costText;
              $details[] = $detail;
          }

          $detail['name'] = "Reiseroute";
          $detail['content'] = $attributes['trip'];
          $details[] = $detail;

          $detail['name'] = "Unterkunft/Verpflegung";
          $detail['content'] = $attributes['lodge'];
          $details[] = $detail;

          if($attributes['meeting_date'] != "0000-00-00" || $attributes['meeting'] != "Keine")
          {
              if($attributes['meeting_date'] != "0000-00-00")
              {
                  $meetingDateTime = date_create_from_format('!Y-m-d H:i:s', $attributes['meeting_date']." ".$attributes['meeting_time'])->format('d.m.Y, H:i');
                  $detail['content'] = $attributes['meeting'].": ".$meetingDateTime." Uhr / ".$attributes['meeting_place'];
              }
              else
              {
                  $detail['content'] = $attributes['meeting'];
              }

              $detail['name'] = "Besprechung";
              $details[] = $detail;
          }

          $detail['name'] = "Karte";
          $detail['content'] = $attributes['map'];
          $details[] = $detail;

          if($attributes['appointment_date'] != "0000-00-00")
          {
              $meetingDateTime = date_create_from_format('!Y-m-d H:i:s', $attributes['appointment_date']." ".$attributes['appointment_time'])->format('d.m.Y, H:i');
              $detail['name'] = "Treffpunkt";
              $detail['content'] = $meetingDateTime." Uhr / ".$attributes['appointment_place'];
              $details[] = $detail;
          }

          $detail['name'] = "Route / Details";
          $detail['content'] = $attributes['description'];
          $details[] = $detail;

          $detail['name'] = "Ausrüstung";
          $detail['content'] = $attributes['equipment'];
          $details[] = $detail;

          if(intval($attributes['participants_max']) > 0)
          {
              $detail['name'] = "Max TN";
              $detail['content'] = $attributes['participants_max'];
              $details[] = $detail;
          }

          $detail['name'] = "Zusatzinfo";
          $detail['content'] = $attributes['text'];
          $details[] = $detail;

          if($attributes['register_end_date'] != "0000-00-00")
          {
              $registrationDate = date_create_from_format('!Y-m-d', $attributes['register_end_date']);
              $detail['name'] = "Anmeldung";
              $detail['content'] = "bis ".$registrationDate->format('d.m.Y');
              $details[] = $detail;
          }

          $item['detail'] = $details;

          $items[] = $item;
      }

      return $items;
    }

    private function getLead($addresses = [])
    {
        $lead = "";

        $nAddr = count($addresses);
        if($nAddr)
        {
            if($nAddr > 1)
            {
                foreach ($addresses as $address)
                {
                    if ($address === reset($addresses))
                    {
                        $lead = $address['@attributes']['fname']." ".$address['@attributes']['lname'];
                    }
                    else
                    {
                        $lead = $lead.", ".$address['@attributes']['fname']." ".$address['@attributes']['lname'];
                    }
                }
            }
            else
            {
                $lead = $addresses['@attributes']['fname']." ".$addresses['@attributes']['lname'];
            }
        }

        return $lead;
    }

    private function getLeadAddress($addresses = [])
    {
        $lead = [];

        $nAddr = count($addresses);
        if($nAddr)
        {
            if($nAddr > 1)
            {
                $data = $addresses[0]['@attributes'];
            }
            else
            {
                $data = $addresses['@attributes'];
            }

            $item = $data['fname']." ".$data['lname'];
            $lead[] = $item;

            $item = $data['address'];
            $lead[] = $item;

            $item = $data['zip']." ".$data['city'];
            $lead[] = $item;

            $item = "Telefon ".$data['phone'];
            $lead[] = $item;

            $item = "Mobile ".$data['mobile'];
            $lead[] = $item;

            $item = "<a href=\"mailto:".$data['email']."\">Mail</a>";
            $lead[] = $item;
        }

        return $lead;
    }

    private function getStatusStr($status)
    {
        switch($status)
        {
            case 0:
                return "";
            case 1:
                return "neues Datum";
            case 2:
                return "abgesagt";
            case 3:
                return "ausgebucht";
            default:
                return "";
        }
    }

    private function dateDifference($date_1 , $date_2)
    {
        $datetime1 = date_create_from_format('!Y-m-d', $date_1);
        $datetime2 = date_create_from_format('!Y-m-d', $date_2);
        $checkDate = date_create_from_format('!Y-m-d', "1970-1-1");

        if($datetime1 < $checkDate || $datetime2 < $checkDate)
        {
            return "1 Tag";
        }

        return (date_diff($datetime2, $datetime1)->format('%a') + 1)." Tage";
    }
}
